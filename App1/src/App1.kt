fun main(args: Array <String>) {

    print("Enter number a:")
    var a:Int = readLine()!!.toInt()
    print("\n")
    print("Enter number b:")
    var b:Int = readLine()!!.toInt()

    println("Entered number a = "+ a)
    println("Entered number b = "+ b)

    var c:Int = a
    a = b
    b = c

    println("Swapped number a = "+ a)
    println("Swapped number b = "+ b)
}